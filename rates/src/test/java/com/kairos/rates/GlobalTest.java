package com.kairos.rates;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;

@SpringBootTest
@AutoConfigureMockMvc
public class GlobalTest {

	@Autowired
	private MockMvc mvc;

	@Test
	public void checkEndPointTest1() throws Exception {

		LinkedMultiValueMap<String, String> requestParams = new LinkedMultiValueMap<>();
		requestParams.add("date", "2020-06-10 10:00:00");
		requestParams.add("productId", "35455");
		requestParams.add("brandId", "1");

		mvc.perform(get("/rates").params(requestParams).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());

	}

}
