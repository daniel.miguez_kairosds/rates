package com.kairos.rates.ratesadapter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.kairos.rates.application.persist.RatesRepositoryAdapter;
import com.kairos.rates.ratesboundary.domain.RatesDto;
import com.kairos.rates.ratesboundary.drivenport.IRatesRepository;

import javassist.NotFoundException;

@SpringBootTest
public class RatesAdapterTest {
	
	@Autowired
	RatesRepositoryAdapter RatesRepository;
	

	@Test
	public void whenGetRatesProduct_thenGetStatusNotFound() throws NotFoundException, ParseException {
				
		NotFoundException ex = assertThrows(NotFoundException.class,()-> {
			
			RatesRepository.getRatesByDateProductBrand(new SimpleDateFormat("yyyy-MM-dd HH:mm").parse("2020-06-10 10:00:00"), 35455, 1);
			
		});

	    assertThat(ex).isInstanceOf(NotFoundException.class);
		
	}
	
    @Test
    public void whenGetRatesProduct_thenGetRates() throws NotFoundException, ParseException {
    	
    	RatesDto rate = RatesRepository.getRatesByDateProductBrand(new SimpleDateFormat("yyyy-MM-dd HH:mm").parse("2020-06-14 10:00:00"), 35455, 1);
    	
    	
    	assertThat(rate.getProductId(), equalTo(35455));
    	assertThat(rate.getBrandId(), equalTo(1));
    	assertTrue(fechas(rate.getStartDate()));    	
    	
    }
    



	private boolean fechas(Date date) throws ParseException {
		return (date.getTime() - new SimpleDateFormat("yyyy-MM-dd HH:mm").parse("2020-06-14 00:00:00").getTime()) < 1000;
	}

	
}
