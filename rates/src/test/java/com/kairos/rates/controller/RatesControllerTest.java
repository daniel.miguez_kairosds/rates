package com.kairos.rates.controller;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.util.LinkedMultiValueMap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kairos.rates.application.controller.dto.RatesResponseDto;

@SpringBootTest
@AutoConfigureMockMvc
public class RatesControllerTest {
	
	@Autowired
	private MockMvc mvc;
	
	@Autowired
	private ObjectMapper objectMapper;

    static SimpleDateFormat sdf;
    
    @BeforeAll
    public static void initFormat() {
    	sdf = new SimpleDateFormat();
        sdf.applyPattern("dd-MM-yyyy HH:mm:ss");
    }

	
	@Test
	public void checkEndPointTest1() throws Exception {
		
		LinkedMultiValueMap<String, String> requestParams = new LinkedMultiValueMap<>();
				requestParams.add("date", "2020-06-14 10:00:00");
				requestParams.add("productId", "35455");
				requestParams.add("brandId", "1");
						
		ResultActions result = checkEndpoint(requestParams);
		assertEndPoint(result,1,sdf.parse("2020-06-14 00:00:00"),35.5);	
		
	
	}

	@Test
	public void checkEndPointTest2() throws Exception {
		
		LinkedMultiValueMap<String, String> requestParams = new LinkedMultiValueMap<>();
		requestParams.add("date", "2020-06-14 16:00:00");
		requestParams.add("productId", "35455");
		requestParams.add("brandId", "1");

		ResultActions result = checkEndpoint(requestParams);
		assertEndPoint(result,2,sdf.parse("2020-06-14 15:00:00"),25.45);
		
	}
	
	
	@Test
	public void checkEndPointTest3() throws Exception {
		
		LinkedMultiValueMap<String, String> requestParams = new LinkedMultiValueMap<>();
		requestParams.add("date", "2020-06-14 21:00:00");
		requestParams.add("productId", "35455");
		requestParams.add("brandId", "1");
				
		ResultActions result = checkEndpoint(requestParams);
		assertEndPoint(result,1,sdf.parse("2020-06-14 00:00:00"),35.5);	
		
	}
	
	
	@Test
	public void checkEndPointTest4() throws Exception {
		
		LinkedMultiValueMap<String, String> requestParams = new LinkedMultiValueMap<>();
		requestParams.add("date", "2020-06-15 10:00:00");
		requestParams.add("productId", "35455");
		requestParams.add("brandId", "1");
		
		ResultActions result = checkEndpoint(requestParams);
		assertEndPoint(result,3,sdf.parse("2020-06-15 00:00:00"),30.5);
		
	}
	
	@Test
	public void checkEndPointTest5() throws Exception {
		
		LinkedMultiValueMap<String, String> requestParams = new LinkedMultiValueMap<>();
		requestParams.add("date", "2020-06-16 21:00:00");
		requestParams.add("productId", "35455");
		requestParams.add("brandId", "1");

		ResultActions result = checkEndpoint(requestParams);
		assertEndPoint(result,4,sdf.parse("2020-06-15 16:00:00"),38.95);

		
	}
	
	
	private void assertEndPoint(ResultActions result,int rate, Date date, double price) throws UnsupportedEncodingException, JsonProcessingException, JsonMappingException, ParseException {
		
		String contentAsString = result.andReturn().getResponse().getContentAsString();
		RatesResponseDto rateResponse = objectMapper.readValue(contentAsString, RatesResponseDto.class);
		
    	assertThat(rateResponse.getProductId(), equalTo(35455));
    	assertThat(rateResponse.getBrandId(), equalTo(1));
    	assertThat(rateResponse.getRate(), equalTo(rate));
    	assertThat(rateResponse.getPrice(), equalTo(price));
    	assertTrue(fechas(date));    	

	}
	
	private boolean fechas(Date date) throws ParseException {
		return (date.getTime() - new SimpleDateFormat("yyyy-MM-dd HH:mm").parse("2020-06-14 00:00:00").getTime()) < 1000;
	}
	
	private ResultActions checkEndpoint(LinkedMultiValueMap<String, String> requestParams) throws Exception {
		return mvc.perform(get("/rates")
					.params(requestParams)					
					.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk());
	}

	
	
}
