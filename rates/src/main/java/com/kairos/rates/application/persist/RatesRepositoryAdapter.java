package com.kairos.rates.application.persist;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kairos.rates.infrastructure.model.Prices;
import com.kairos.rates.infrastructure.repository.PricesRepository;
import com.kairos.rates.ratesboundary.domain.RatesDto;
import com.kairos.rates.ratesboundary.drivenport.IRatesRepository;

import javassist.NotFoundException;

@Component
public class RatesRepositoryAdapter implements IRatesRepository{
	
	@Autowired
	private PricesRepository pricesRepository;
	
	public RatesDto getRatesByDateProductBrand(Date date, Integer productId, Integer brandId) throws NotFoundException {
		
		Optional<List<Prices>> price = pricesRepository.getRatesByPriority(date, productId, brandId);
		
		if(price.isPresent() && !price.get().isEmpty())
			return this.toRatesDto(price.get().get(0));
		
		throw new NotFoundException("Rate for product: " + productId + " not found" );
	}
		
	
	private RatesDto toRatesDto(Prices price) {
		return new RatesDto(price.getPriceList(), price.getBrandId(), price.getStartDate(), price.getEndDate(), price.getProductId(), price.getPriority(), price.getPrice(), price.getCurrency(), price.getLastUpdate(), price.getLastUpdateBy());
	}
	
	@Override
	public List<Prices> getRates() {
		return pricesRepository.findAll();
	}

}
