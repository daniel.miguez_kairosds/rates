package com.kairos.rates.application.controller.dto;

import java.util.Date;

import com.kairos.rates.ratesboundary.domain.RatesDto;

public class RatesResponseDto {
	
	private Integer productId;
	private Integer brandId;
	private Integer rate;
	private Date applicationDate;
	private Double price;
	
	public static RatesResponseDto fromRateDto(RatesDto rate) {		
		return new RatesResponseDto(rate.getProductId(), rate.getBrandId(), rate.getPriceList(), rate.getStartDate(), rate.getPrice());		
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getBrandId() {
		return brandId;
	}

	public void setBrandId(Integer brandId) {
		this.brandId = brandId;
	}

	public Integer getRate() {
		return rate;
	}

	public void setRate(Integer rate) {
		this.rate = rate;
	}

	public Date getApplicationDate() {
		return applicationDate;
	}

	public void setApplicationDate(Date applicationDate) {
		this.applicationDate = applicationDate;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public RatesResponseDto(Integer productId, Integer brandId, Integer rate, Date applicationDate, Double price) {
		super();
		this.productId = productId;
		this.brandId = brandId;
		this.rate = rate;
		this.applicationDate = applicationDate;
		this.price = price;
	}
	
	
	

}
