package com.kairos.rates.application.controller;

import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kairos.rates.application.controller.dto.RatesResponseDto;
import com.kairos.rates.ratesboundary.driverport.IGetRateUseCase;

import javassist.NotFoundException;

@RestController
public class RatesController {

	private final static Logger LOGGER = Logger.getLogger("RatesController");

	@Autowired
	private IGetRateUseCase ratesService;

	@GetMapping("/rates")
	public ResponseEntity<RatesResponseDto> getRates(@RequestParam(name = "date") String date,
														  @RequestParam(name = "productId") Integer productId, 
														  @RequestParam(name = "brandId") Integer brandId){
			
			try {
				return ResponseEntity.ok( 
						RatesResponseDto.fromRateDto(
								ratesService.getRatesDto( new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(date), productId, brandId)));
			} catch (NotFoundException e){				
				LOGGER.log(Level.INFO, e.getMessage());
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}catch (Exception e) {			
				e.printStackTrace();
				return new ResponseEntity<>( HttpStatus.INTERNAL_SERVER_ERROR);
			}


		
	}
}
