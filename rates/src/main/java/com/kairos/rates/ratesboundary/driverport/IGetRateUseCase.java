package com.kairos.rates.ratesboundary.driverport;

import java.util.Date;

import com.kairos.rates.ratesboundary.domain.RatesDto;

import javassist.NotFoundException;


public interface IGetRateUseCase {
	public RatesDto getRatesDto(Date fechaAplicacion, Integer productId, Integer brandId) throws NotFoundException;
}
