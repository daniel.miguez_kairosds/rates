package com.kairos.rates.ratesboundary.drivenport;

import java.util.List;

import com.kairos.rates.infrastructure.model.Prices;

public interface IRatesRepository {

	public List<Prices> getRates();
}
