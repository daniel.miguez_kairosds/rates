package com.kairos.rates.ratesboundary.domain;

import java.util.Date;
import java.util.Objects;

import com.kairos.rates.infrastructure.model.Prices;

public class RatesDto {
	
	private Integer priceList;
	private Integer brandId;
	private Date startDate;
	private Date endDate;
	private Integer productId;
	private Integer priority;
	private Double price;
	private String currency;
	private Date lastUpdate;
	private String lastUpdateBy;	
	
	
	public RatesDto(Integer priceList, Integer brandId, Date startDate, Date endDate, Integer productId, Integer priority,
			Double price, String currency, Date lastUpdate, String lastUpdateBy) {
		this.priceList = priceList;
		this.brandId = brandId;
		this.startDate = startDate;
		this.endDate = endDate;
		this.productId = productId;
		this.priority = priority;
		this.price = price;
		this.currency = currency;
		this.lastUpdate = lastUpdate;
		this.lastUpdateBy = lastUpdateBy;
	}
	
	public Integer getBrandId() {
		return brandId;
	}
	public void setBrandId(Integer brandId) {
		this.brandId = brandId;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Integer getPriceList() {
		return priceList;
	}
	public void setPriceList(Integer priceList) {
		this.priceList = priceList;
	}
	public Integer getPriority() {
		return priority;
	}
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getLastUpdateBy() {
		return lastUpdateBy;
	}
	public void setLastUpdateBy(String lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}

	@Override
	public int hashCode() {
		return Objects.hash(brandId, currency, endDate, lastUpdate, lastUpdateBy, price, priceList, priority, productId,
				startDate);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RatesDto other = (RatesDto) obj;
		return Objects.equals(brandId, other.brandId) && Objects.equals(currency, other.currency)
				&& Objects.equals(endDate, other.endDate) && Objects.equals(lastUpdate, other.lastUpdate)
				&& Objects.equals(lastUpdateBy, other.lastUpdateBy) && Objects.equals(price, other.price)
				&& Objects.equals(priceList, other.priceList) && Objects.equals(priority, other.priority)
				&& Objects.equals(productId, other.productId) && Objects.equals(startDate, other.startDate);
	}
	
	
	public static RatesDto toRatesDto(Rate rate) {
		return new RatesDto(rate.getPriceList(), rate.getBrandId(), rate.getStartDate(), rate.getEndDate(), rate.getProductId(), rate.getPriority(), rate.getPrice(), rate.getCurrency(), rate.getLastUpdate(), rate.getLastUpdateBy());
	}

	
}
