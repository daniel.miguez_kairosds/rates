package com.kairos.rates.ratesboundary.driverport;

import java.util.Comparator;
import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kairos.rates.ratesboundary.domain.Rate;
import com.kairos.rates.ratesboundary.domain.RatesDto;
import com.kairos.rates.ratesboundary.drivenport.IRatesRepository;

import javassist.NotFoundException;

@Service
public class GetRateUseCaseImpl implements IGetRateUseCase {
	
	@Autowired
	private IRatesRepository rateRepository;
	
	
	@Override
	public RatesDto getRatesDto(Date date, Integer productId, Integer brandId) throws NotFoundException {
		
		Optional<Rate> rates =  Rate.toRates(rateRepository.getRates()).stream()
				.sorted(Comparator.comparing(Rate::getPriority).reversed())				
				.filter(rate -> filterByProductIdBrandIdAndDate(date, productId, brandId, rate))
				.findFirst();
		
		if(rates.isPresent())
			return RatesDto.toRatesDto(rates.get());
		
		throw new NotFoundException("Rate for product: " + productId + " not found" );
		
	}


	private boolean filterByProductIdBrandIdAndDate(Date date, Integer productId, Integer brandId, Rate rate) {
		return rate.getBrandId().compareTo(brandId)==0 && rate.getProductId().compareTo(productId) == 0 && date.after(rate.getStartDate()) && date.before(rate.getEndDate());
	}
	
}
