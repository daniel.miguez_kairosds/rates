package com.kairos.rates.infrastructure.model;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Prices {
	
	@Id
	private Integer priceList;
	private Integer brandId;
	private Date startDate;
	private Date endDate;
	private Integer productId;
	private Integer priority;
	private Double price;
	private String currency;
	private Date lastUpdate;
	private String lastUpdateBy;
	
	public Integer getBrandId() {
		return brandId;
	}
	public void setBrandId(Integer brandId) {
		this.brandId = brandId;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Integer getPriceList() {
		return priceList;
	}
	public void setPriceList(Integer priceList) {
		this.priceList = priceList;
	}
	public Integer getPriority() {
		return priority;
	}
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getLastUpdateBy() {
		return lastUpdateBy;
	}
	public void setLastUpdateBy(String lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}
	@Override
	public int hashCode() {
		return Objects.hash(brandId, currency, endDate, lastUpdate, lastUpdateBy, price, priceList, priority, productId,
				startDate);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Prices other = (Prices) obj;
		return Objects.equals(brandId, other.brandId) && Objects.equals(currency, other.currency)
				&& Objects.equals(endDate, other.endDate) && Objects.equals(lastUpdate, other.lastUpdate)
				&& Objects.equals(lastUpdateBy, other.lastUpdateBy) && Objects.equals(price, other.price)
				&& Objects.equals(priceList, other.priceList) && Objects.equals(priority, other.priority)
				&& Objects.equals(productId, other.productId) && Objects.equals(startDate, other.startDate);
	}
	
	
	
}
