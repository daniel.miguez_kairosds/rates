package com.kairos.rates.infrastructure.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.kairos.rates.infrastructure.model.Prices;

@Repository
public interface PricesRepository extends JpaRepository<Prices, Integer>{

	@Query("select p from Prices p where p.productId = :productId and p.startDate <= :date and p.endDate >= :date and p.brandId = :brandId order by p.priority desc")
	Optional<List<Prices>> getRatesByPriority(@Param("date") Date date, @Param("productId") Integer productId, @Param("brandId") Integer brandId);

}
